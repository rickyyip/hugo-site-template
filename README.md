# Hugo Site Template

## Features

- Tailwind CSS
- Fontawesome 5
- Alpine.js application framework
- Fuse.js local content search

## Requirements

1. Node Package Manager (NPM) 14 or newer
2. (Optional) Latest Hugo binary

## Installation

1. Clone git repository
2. Run: `npm install`
3. Start a local server: `npm run server` or `hugo server`
4. Build site: `npm run build` or `hugo`

Note: `npm run build` will clear all contents in `./public` directory.
