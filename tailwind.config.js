module.exports = {
  purge: {
    enabled: process.env.HUGO_ENVIRONMENT === "production",
    content: [
      "./hugo_stats.json",
      "./layouts/**/*.html",
      "./content/**/*.md",
      "./content/**/*.html",
      "./assets/js/**/*.js",
      "./archetypes/**/*.md",
    ],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
