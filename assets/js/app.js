function scrollFunction() {
  //Get the button
  let topButton = document.getElementById("backToTopBtn");

  if (
    document.body.scrollTop > 50 ||
    document.documentElement.scrollTop > 50
  ) {
    topButton.style.display = "block";
  } else {
    topButton.style.display = "none";
  }
}

function backToTop(){
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
